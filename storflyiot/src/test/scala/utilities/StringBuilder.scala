package utilities

object StringBuilder {
  def randomString(length: Int) = scala.util.Random.alphanumeric.take(length).mkString
}
