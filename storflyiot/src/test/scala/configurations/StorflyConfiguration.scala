package configurations

import com.typesafe.config.ConfigFactory

object StorflyConfiguration {
  val STORFLY_CONFIGURATION_FILE = "storflyiot.conf"

  val STORFLY_DEVICE_IP = ConfigFactory.load(StorflyConfiguration.STORFLY_CONFIGURATION_FILE).getString("storfly.device.ip")
  val STORFLY_DEVICE_TOKEN = ConfigFactory.load(StorflyConfiguration.STORFLY_CONFIGURATION_FILE).getString("storfly.device.token")
}
