package simulations

import configurations.StorflyConfiguration
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import utilities.StringBuilder

import scala.concurrent.duration._

class Data_Integrity_Concurrently_Using_4_Streams_With_Payload_4B extends Simulation {

  val runtime = 5.days // 10.seconds // 10.minutes // 120.hours
  val sample_size_in_bytes = 4
  val delayBetweenRequests = 150.milliseconds

  var payload = ""
  var expectedContentOfTheLatestSegment = ""

  val httpProtocol = http.baseURL("https://%s:511".format(StorflyConfiguration.STORFLY_DEVICE_IP))
    .authorizationHeader("Bearer " + StorflyConfiguration.STORFLY_DEVICE_TOKEN)
    .contentTypeHeader("application/json")

  def data_integrity_scenario(iScenarioName: String, iStreamId: String) = {
    scenario(iScenarioName)
      .during(runtime) {
        payload = StringBuilder.randomString(sample_size_in_bytes)
        expectedContentOfTheLatestSegment = "[\"%s\"]".format(payload)

        exec(http("submit a sample to the stream %s".format(iStreamId)) // Write data to a particular stream with payload set to 4 bytes
          .post("/api/v1/streams/%s".format(iStreamId))
          .body(StringBody(s"""{ "data": "$payload" }""")).asJSON
          .check(status.is(200))
          .check(bodyString.is("true"))
        ).pause(delayBetweenRequests) // Delay between requests
          .exec(http("read the latest segment of the stream " + iStreamId) // Read the latest segments and verify data
          .get("/api/v1/streams/%s/segments/latest".format(iStreamId))
          .check(status.is(200))
          .check(jsonPath("$.content").is(expectedContentOfTheLatestSegment))
        )
      }
  }

  val user_01 = data_integrity_scenario(iScenarioName = "scn_01", iStreamId = "di1")
  val user_02 = data_integrity_scenario(iScenarioName = "scn_02", iStreamId = "di2")
  val user_03 = data_integrity_scenario(iScenarioName = "scn_03", iStreamId = "di3")
  val user_04 = data_integrity_scenario(iScenarioName = "scn_04", iStreamId = "di4")

  setUp(
    user_01.inject(atOnceUsers(1)),
    user_02.inject(atOnceUsers(1)),
    user_03.inject(atOnceUsers(1)),
    user_04.inject(atOnceUsers(1))
  ).protocols(httpProtocol)
}