package simulations

import configurations.StorflyConfiguration
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import utilities.StringBuilder

import scala.concurrent.duration._

class Write_Read_Concurrently_Using_8_Connections_With_Payload_1KB extends Simulation {

  val runtime = 5.days // 10.seconds // 10.minutes // 120.hours
  val sample_size_in_bytes = 1024
  val delayBetweenRequests = 150.milliseconds
  val delayBeforeReading = 1.minutes
  var payload = ""

  val httpProtocol = http.baseURL("https://%s:511".format(StorflyConfiguration.STORFLY_DEVICE_IP))
    .authorizationHeader("Bearer " + StorflyConfiguration.STORFLY_DEVICE_TOKEN)
    .contentTypeHeader("application/json")

  def write_scenario(iScenarioName: String, iStreamId: String) = {
    scenario(iScenarioName)
      .during(runtime) {
        payload = StringBuilder.randomString(length = sample_size_in_bytes)
        exec(http("submit a sample to the stream %s".format(iStreamId))
          .post("/api/v1/streams/%s".format(iStreamId))
          .body(StringBody(s"""{ "data": "$payload" }""")).asJSON
          .check(status.is(200))
          .check(bodyString.is("true"))
        ).pause(delayBetweenRequests)
      }
  }

  def read_all_scenario(iScenarioName: String, iStreamId: String, iDelayBeforeStarting: Duration) = {
    scenario(iScenarioName)
      .pause(iDelayBeforeStarting)
      .exec(http("Get all segments of the stream " + iStreamId)
        .get("/api/v1/streams/%s/segments/all/100".format(iStreamId))
        .check(status.is(200))
        .check(jsonPath("$.paging.next").saveAs("pagingNext"))
      )
      .pause(delayBetweenRequests)
      .during(runtime) {
        exec(http("Get segments forward on all segments of the stream %s".format(iStreamId))
          .get("${pagingNext}")
          .check(status.is(200))
          .check(jsonPath("$.paging.next").saveAs("pagingNext"))
        ).pause(delayBetweenRequests)
      }
  }

  // create users for writing
  val writer_01 = write_scenario(iScenarioName = "write_01", iStreamId = "sWR1")
  val writer_02 = write_scenario(iScenarioName = "write_02", iStreamId = "sWR2")
  val writer_03 = write_scenario(iScenarioName = "write_03", iStreamId = "sWR3")
  val writer_04 = write_scenario(iScenarioName = "write_04", iStreamId = "sWR4")
  // create users for reading
  val reader_01 = read_all_scenario(iScenarioName = "read_01", iStreamId = "sWR1", iDelayBeforeStarting = delayBeforeReading)
  val reader_02 = read_all_scenario(iScenarioName = "read_02", iStreamId = "sWR2", iDelayBeforeStarting = delayBeforeReading)
  val reader_03 = read_all_scenario(iScenarioName = "read_03", iStreamId = "sWR3", iDelayBeforeStarting = delayBeforeReading)
  val reader_04 = read_all_scenario(iScenarioName = "read_04", iStreamId = "sWR4", iDelayBeforeStarting = delayBeforeReading)

  setUp(
    // inject to write
    writer_01.inject(atOnceUsers(1)),
    writer_02.inject(atOnceUsers(1)),
    writer_03.inject(atOnceUsers(1)),
    writer_04.inject(atOnceUsers(1)),
    // inject to read
    reader_01.inject(atOnceUsers(1)),
    reader_02.inject(atOnceUsers(1)),
    reader_03.inject(atOnceUsers(1)),
    reader_04.inject(atOnceUsers(1))
  ).protocols(httpProtocol)
}