package simulations

import configurations.StorflyConfiguration
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import utilities.StringBuilder

import scala.concurrent.duration._

class Write_Concurrently_Using_8_Connections_With_Payload_128B extends Simulation {

  val runtime = 5.days // 10.seconds // 10.minutes // 120.hours
  val sample_size_in_bytes = 128
  val delayBetweenRequests = 150.milliseconds
  var payload = StringBuilder.randomString(sample_size_in_bytes)

  val httpProtocol = http.baseURL("https://%s:511".format(StorflyConfiguration.STORFLY_DEVICE_IP))
    .authorizationHeader("Bearer " + StorflyConfiguration.STORFLY_DEVICE_TOKEN)
    .contentTypeHeader("application/json")

  def write_samples_to_a_particular_stream_scenario(iScenarioName: String, iStreamId: String) = {
    scenario(iScenarioName)
      .during(runtime) {
        exec(http("Submit a sample to the stream %s" format (iStreamId))
          .post("/api/v1/streams/" + iStreamId)
          .body(StringBody(s"""{"data":"$payload"}"""))
          .check(status.is(200))
          .check(bodyString.is("true"))
        ).pause(delayBetweenRequests)
      }
  }

  val user_01 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_01", iStreamId = "st1")
  val user_02 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_02", iStreamId = "st2")
  val user_03 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_03", iStreamId = "st3")
  val user_04 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_04", iStreamId = "st4")
  val user_05 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_05", iStreamId = "st5")
  val user_06 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_06", iStreamId = "st6")
  val user_07 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_07", iStreamId = "st7")
  val user_08 = write_samples_to_a_particular_stream_scenario(iScenarioName = "user_08", iStreamId = "st8")

  setUp(
    user_01.inject(atOnceUsers(1)),
    user_02.inject(atOnceUsers(1)),
    user_03.inject(atOnceUsers(1)),
    user_04.inject(atOnceUsers(1)),
    user_05.inject(atOnceUsers(1)),
    user_06.inject(atOnceUsers(1)),
    user_07.inject(atOnceUsers(1)),
    user_08.inject(atOnceUsers(1))
  ).protocols(httpProtocol)
}


