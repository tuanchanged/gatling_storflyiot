############################################################

### HOW TO RUN THE GATLING SCRIPTS BY COMMAND PROMPT - BUILD TOOL ###

#Step 1: Open the command prompt from the gatling project directory and type: mvn gatling:test -Dgatling.simulationClass=pathToYourSimulationclass

############################################################


############################################################
### HOW TO RUN THE GATLING SCRIPTS - RECORD & PLAYBACK ###

#Step 1: Download the gatling tool here https://gatling.io/download/

#Step 2: Copy scripts to the folder named simulations (.\user-files\simulations)

#Step 3: Open the script and Edit the 2 fields as device_ip, device_token corresponding to the ip and token of your device

#Step 4: Open the command prompt from the location gatling tool and run the script by command line as the example below:

> C:\Users\gatling-charts-highcharts-bundle-2.3.1>bin\gatling.bat
GATLING_HOME is set to "C:\Users\tuan.nguyen\Desktop\gatling-charts-highcharts-bundle-2.3.1"
JAVA = ""C:\Program Files\Java\jdk1.8.0_144\bin\java.exe""
Choose a simulation number:
     [0] Write_Concurrently_To_16_Streams_With_Payload_128_Bytes
     [1] Write_Concurrently_To_16_Streams_With_Payload_1_Kilobytes
     [2] Write_Concurrently_To_4_Streams_With_Payload_1_Kilobytes
     [3] Write_Concurrently_To_8_Streams_With_Payload_128_Bytes
     [4] Write_Concurrently_To_8_Streams_With_Payload_1_Kilobytes

#Step 5: Choose a number, then press enter key 2 times to continuous

#Step 6: Get the results in the folder named results

############################################################